# VS Code

## 扩展

- *Auto Close Tag* 自动闭合HTML标签
- *Auto Rename Tag* 修改标签时同步修改开始标签和结束标签
- *Bracket Pair Colorizer* 彩色括号匹配
- *change-case* 快速修改突出显示的选中文本的命名格式。
- *Chinese (Simplified) Language Pack for Visual Studio Code* 中文语言包
  - `Ctrl+Shift+P` 打开命令面板，输入`config` 选择配置语言命令，在`locale.json` 中添加`“locale”:"zh-cn"` 
- *Code Runner* 
- *Code Spell Checker* 检查单词拼写
- *Color Info* 
- *Debugger for Chrome* 
- *Debugger for Firefox* 
- *Document This* 在TypeScript和JavaScript文件中自动生成JSDoc注释
  - `Ctrl+Alt+D` and again `Ctrl+Alt+D` 
- *Dracula Official* 主题
- *Emoji Code* 
- *filesize* 
- *GitLens -- Git supercharged* 
- *HTML CSS Support* 
- *htmltagwrap* 
- *JavaScript (ES6) code snippets* 用代码片段加快ES6开发速度
- *JSON Tools* 压缩JSON
- *Material Theme* 主题
- *npm* 
- *npm Intellisense* 在import导入语句中自动完成npm模块
- *open in browser* 在浏览器打开
- *Partial Diff* 对比文件
- *Path Intellisense* 路径提示
- *PostCss Sorting* 排序css
- *Project Manager* 项目管理器，可以在编辑器中快速切换项目。
- *Regex Previewer* 
- *Sass* 
- *Settings Sync* 设置同步，
- *StandardJS - JavaScript Standard Style* 
  - `npm install standard --global` 
  - `standard --fix` 自动格式化
  - README文件中的专属徽章 
    - [![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)  
    - [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)   

- *SVG Viewer* 
- *TODO Highlight* 高亮todo
- *Vetur* 
- *View Node Package* 
- *VSCode Great Icons* 侧边栏图标
- *vscode-database* 
- *Vue 2 Snippets* 



## 设置

```
{
    "workbench.colorTheme": "Dracula",
    "workbench.iconTheme": "vscode-great-icons",
    "editor.formatOnSave": true,
    "files.autoSave": "onWindowChange",
    "javascript.validate.enable": false,
    "standard.validate": [
        "javascript",
        "javascriptreact",
        {
            "language": "html",
            "autoFix": true
        }
    ],
    "standard.options": {
        "plugin": [
            "html"
        ]
    },
    "files.associations": {
        "*.vue": "html"
    },
    "standard.autoFixOnSave": true,
    "emmet.triggerExpansionOnTab": true
}
```



## 快捷键

